# Use the official NGINX image as the base image
FROM nginx:latest

# Copy your custom configuration file (if any) into the container
# You can create a custom nginx.conf and place it in the same directory as the Dockerfile
# COPY nginx.conf /etc/nginx/nginx.conf

# Copy your static website files (e.g., HTML, CSS, JS) to the default web directory
COPY index.html /usr/share/nginx/html/

# Expose port 80 to allow web traffic
EXPOSE 80

# Start NGINX server
CMD ["nginx", "-g", "daemon off;"]

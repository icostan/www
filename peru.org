* Peru
** Requirements
*** 1. Peru native operator - fuck international/hybrid operators
*** 2. small operator - half dozen guides
*** 3. not in top 10 on trip advisor - no mainstream operator
*** 4. relative new company, max 10 years old - no OG here
*** 4. environment/communities friendly - bonus
** Operators list
    -https://www.machupicchu.gob.pe/?lang=en
   - https://machupicchutravelguide.com/top-inca-trail-tour-operator-peru/
   - http://www.incatrailperu.com/inca_trail_tour_operators.html
   - http://www.incatrailperu.com/inca_trail_route_options.html
** Inca trail tours
*** 4 days classic Inca trail to Macchu Picchu
    - https://www.salkantaytrekking.com/trekking-in-peru/inca-trail/trek-inca-trail-machu-picchu-cusco-peru/
    - https://www.unitedmice.com/en/inca-trail
    - https://www.incahike.com/inca-trail-hike-4-days.html
    - https://www.sungatetours.com/trips/classic-inca-trail-group-tour/
    - https://www.cusconative.com/tours/classic-inca-trail-to-machu-picchu/
*** 5 days extended Inca trail to Machu Picchu
**** day 2 - at Llulluchapampa
     - https://amazonas-explorer.com/tours/5-day-inca-trail-trek-to-machu-picchu/ :inquire:$1331pp:
     - https://www.explorandes.com/trips/trekking/inca-trail-machu-picchu/ :inquire:
     - https://www.apus-peru.com/tour/inca-trail-trek-5d :site:$1275pp:
     - https://www.enigmaperu.com/en/program/969/classic-inca-trail-to-machupicchu :inquire:$1244:
***** Orange-nation
      tour: https://www.orange-nation.com/5-days-inca-trail-to-machu-picchu-orange-nation/
      spin-off company: https://www.samtravelperu.com/inca-trail-tour-5-day/
      price: 747 USD / person
**** day 2 - at Pacaymayo
     - https://www.alpacaexpeditions.com/inca-trail-expedition-5d4n/
     - https://www.andeanadventuresperu.com/en/tour/extended-inca-trail-to-machu-picchu/
     - https://www.waykitrek.net/inca-trail/inca-trail-5-days/ :email:camping:$1155pp:
     - https://www.llamapath.com/en/inca-trail-special-service :email:$925pp:
*** 5 days Salkantay (4600m) to Maccu Picchu
    - https://www.alltrekperu.com/tour/salkantay-trek-machu-picchu
    - https://www.salkantaytrekking.com/trekking-in-peru/cusco/trekking-machu-picchu-salkantay-llactapata/
*** 5 days AmaruWatana (4400m) to Macchu Picchu
    - https://andinatravel.com/tours/premium-inca-trail-quillatambo-to-machu-picchu-5d4n/
** Amazon jungle tours
*** info
   - https://tourthetropics.com/guides/top-iquitos-amazon-lodges/
   - https://www.perunorth.com/amazon-lodges-iquitos-peru-comparison
   - https://www.tripadvisor.com/Hotels-g10401576-c3-zff17-Iquitos_Amazon_Region_Loreto_Region-Hotels.html
*** filters
   - no walkways (or the less, the better)
   - terra ferme (high ground, not flooded during the high water season)
   - local owned lodge
*** lodges
   - Tahuayo Lodge - https://perujungle.com/the-lodge/ (FAR) - $220pp
   - Otorongo Lodge - https://tourthetropics.com/tours/otorongo-lodge/ (no bungalows, only building with rooms)
   - Explorama https://explorama.com/ - American company, high end
     - ExplorNapo Lodge (canopy walkway)
     - Ceiba Tops Lodge
   - https://heliconialodge.com.pe/ (bungalows over a swamp, pool) - $140pn
   - https://treehouselodge.com/ (high end, unique experience) - $240pn
   - Sinchicuy lodge - https://www.perunorth.com/sinchicuy-amazon-lodge-iquitos (close to Iquitos), 4D2N - $394
   - https://libertad-jungle-lodge.com/ (low-cost, community lodge)
   - https://www.lupunajungletours.com/ (walkways between buildings, flood?) - 4D3N: $300
   - http://amazonjourneyslodge.com/ (walkways between buildings, flood?)
   - [[https://www.booking.com/hotel/pe/jacamar-lodge-expeditions.en-gb.html?aid=390156&label=duc511jc-1FCAsosQFCFmVjb2FtYXpvbmlhLWxvZGdlLWVpcmxIM1gDaMABiAEBmAEJuAEZyAEM2AEB6AEB-AEDiAIBqAIDuALwjsmSBsACAdICJGVlM2NiNWU3LTYxNWQtNGJjMy05MmU2LTEwY2RiODNhZjJlN9gCBeACAQ&sid=ec6b4887c5e3590b9b29bf14af4867aa&atlas_src=sr_iw_btn&checkin=2022-06-02&checkout=2022-06-06&dest_id=167&dest_type=country&dist=0&group_adults=2&group_children=0&highlighted_blocks=134282303_270675991_2_21_0&nflt=uf%3D-350039&no_rooms=1&room1=A%2CA&sb_price_type=total&type=total&ucfs=1&activeTab=main][Jacamar]] -
   - [[https://www.booking.com/hotel/pe/amazon-wonder-expeditions.en-gb.html?aid=390156&label=duc511jc-1FCAsosQFCFmVjb2FtYXpvbmlhLWxvZGdlLWVpcmxIM1gDaMABiAEBmAEJuAEZyAEM2AEB6AEB-AEDiAIBqAIDuALwjsmSBsACAdICJGVlM2NiNWU3LTYxNWQtNGJjMy05MmU2LTEwY2RiODNhZjJlN9gCBeACAQ&sid=ec6b4887c5e3590b9b29bf14af4867aa&atlas_src=sr_iw_btn&checkin=2022-06-02&checkout=2022-06-06&dest_id=167&dest_type=country&dist=0&group_adults=2&group_children=0&highlighted_blocks=301424301_265001672_2_2_0&nflt=uf%3D-350039&no_rooms=1&room1=A%2CA&sb_price_type=total&type=total&ucfs=1&activeTab=main#_][Amazon Wonder Expedictions]] (basic double rooms) - $420
   - https://amazonoasislodge.business.site/
   - [[https://www.booking.com/hotel/pe/tropical-adventures-amp-expeditions.en-gb.html?aid=390156&label=duc511jc-1FCAYYiwQosQFCEHNlbHZhLWRlLWlxdWl0b3NIM1gDaMABiAEBmAEJuAEZyAEM2AEB6AEB-AENiAIBqAIDuALJiMuSBsACAdICJDZjMWEyMDgyLTcxZGQtNDY5OC05MWRkLWZkNDU4MDRlMmM2NtgCBuACAQ&sid=2fc548bdc58e09c25e507d7d14833da1&all_sr_blocks=151215001_150627218_2_85_0&checkin=2022-06-03&checkout=2022-06-06&dest_id=13532&dest_type=region&dist=0&group_adults=2&group_children=0&hapos=20&highlighted_blocks=151215001_150627218_2_85_0&hpos=20&matching_block_id=151215001_150627218_2_85_0&no_rooms=1&req_adults=2&req_children=0&room1=A%2CA&sb_price_type=total&sr_order=popularity&sr_pri_blocks=151215001_150627218_2_85_0__33707&srepoch=1649996590&srpvid=dc241ed66b100039&type=total&ucfs=1&activeTab=photosGallery][Tropical Adventure and Expeditions]] (basic in the jungle, bar?)
   - [[https://www.booking.com/hotel/pe/aquamazon-lodge-amp-expeditions-all-inclusive.en-gb.html?aid=390156&label=duc511jc-1FCAYYiwQosQFCEHNlbHZhLWRlLWlxdWl0b3NIM1gDaMABiAEBmAEJuAEZyAEM2AEB6AEB-AENiAIBqAIDuALJiMuSBsACAdICJDZjMWEyMDgyLTcxZGQtNDY5OC05MWRkLWZkNDU4MDRlMmM2NtgCBuACAQ&sid=2fc548bdc58e09c25e507d7d14833da1&all_sr_blocks=176707403_348688361_2_21_0&checkin=2022-06-03&checkout=2022-06-06&dest_id=13532&dest_type=region&dist=0&group_adults=2&group_children=0&hapos=14&highlighted_blocks=176707403_348688361_2_21_0&hpos=14&matching_block_id=176707403_348688361_2_21_0&no_rooms=1&req_adults=2&req_children=0&room1=A%2CA&sb_price_type=total&sr_order=popularity&sr_pri_blocks=176707403_348688361_2_21_0__136800&srepoch=1649996590&srpvid=dc241ed66b100039&type=total&ucfs=1&activeTab=main][Yaku Amazon Lodge]] (small, walkways)
   - [[https://www.booking.com/hotel/pe/curuhuinsi-lodge.en-gb.html?aid=390156&label=duc511jc-1FCAYYiwQosQFCEHNlbHZhLWRlLWlxdWl0b3NIM1gDaMABiAEBmAEJuAEZyAEM2AEB6AEB-AENiAIBqAIDuALJiMuSBsACAdICJDZjMWEyMDgyLTcxZGQtNDY5OC05MWRkLWZkNDU4MDRlMmM2NtgCBuACAQ&sid=2fc548bdc58e09c25e507d7d14833da1&all_sr_blocks=156928002_184571108_2_21_0&checkin=2022-06-03&checkout=2022-06-06&dest_id=13532&dest_type=region&dist=0&group_adults=2&group_children=0&hapos=12&highlighted_blocks=156928002_184571108_2_21_0&hpos=12&matching_block_id=156928002_184571108_2_21_0&no_rooms=1&req_adults=2&req_children=0&room1=A%2CA&sb_price_type=total&sr_order=popularity&sr_pri_blocks=156928002_184571108_2_21_0__51000&srepoch=1649996590&srpvid=dc241ed66b100039&type=total&ucfs=1&activeTab=main][Curuhuisini]] (no lodge info)
   - https://www.intillamajunglelodge.pe/en/ (site not loading, walkways)
   - http://junglewolfexpedition.com/ (walkways, double rooms, no separated bungalows)
**** Amak Iquitos                                                  :FAVORITE:
   - [[https://amakperu.com/en/iquitos/]]
   - [[https://www.booking.com/hotel/pe/amak-iquitos.en-gb.html?aid=390156;label=duc511jc-1BCAYYiwQosQFCEHNlbHZhLWRlLWlxdWl0b3NIM1gDaMABiAEBmAEJuAEZyAEM2AEB6AEBiAIBqAIDuALJiMuSBsACAdICJDZjMWEyMDgyLTcxZGQtNDY5OC05MWRkLWZkNDU4MDRlMmM2NtgCBeACAQ;sid=ec6b4887c5e3590b9b29bf14af4867aa][Amak Iquitos Lodge]] -
   - email offer: $379 / pers
   - private boat: $170 / 6pax
**** Muyuna
   - https://muyuna.com/en/home/ - 4D3N: $520
   - offer: $520 / pers
**** Curassow lodge
   - [[http://www.curassowlodge.com/]] - 4D3N: $510
   - offer: $480 / pers
**** Grand Amazon Tours
   - https://grandamazontours.com/lodge/ (Peru+US biologist founders) - 4D2N: $559
   - https://www.booking.com/hotel/pe/amazon-discovery-lodge.en-gb.html?aid=390156&label=duc511jc-1FCAYYiwQosQFCEHNlbHZhLWRlLWlxdWl0b3NIM1gDaMABiAEBmAEJuAEZyAEM2AEB6AEB-AENiAIBqAIDuALJiMuSBsACAdICJDZjMWEyMDgyLTcxZGQtNDY5OC05MWRkLWZkNDU4MDRlMmM2NtgCBuACAQ&sid=2fc548bdc58e09c25e507d7d14833da1&all_sr_blocks=423300406_287973909_2_85_0&checkin=2022-06-03&checkout=2022-06-06&dest_id=13532&dest_type=region&dist=0&group_adults=2&group_children=0&hapos=15&highlighted_blocks=423300406_287973909_2_85_0&hpos=15&matching_block_id=423300406_287973909_2_85_0&no_rooms=1&req_adults=2&req_children=0&room1=A%2CA&sb_price_type=total&sr_order=popularity&sr_pri_blocks=423300406_287973909_2_85_0__134400&srepoch=1649996590&srpvid=dc241ed66b100039&type=total&ucfs=1&activeTab=main
   - crowded, flooded?
   - emailed, no offer
**** Pacaya Samiria
   - https://www.pacayasamiria.com.pe/ (high-end) - 4D3N: $620
   - offer: $535 / pers (discount form $885 / pers)
   - promotion conditions: pay in advance, non-refundable, flexible dates change
**** PumaRinri
   - https://pumarinri.com/en/ (pool, NOT IN Iquitos area) - $250pp
   - offer: $959 / pers
**** Yarapa
   - http://www.yarapa.com/ (no info)
   - offer: $1000 / pers
**** Amazon Garden Eco Lodge                                       :FAVORITE:
   - [[http://amazongardenecolodge.com/]] (boutique with pool, bar, etc)
   - bar next to pool, garden
   - individual bungalows
   - offer: $612 / pers
**** Cumaceba
   - https://www.cumaceba.com/en/
   - https://www.booking.com/hotel/pe/amazonas-botanical-lodge.en-gb.html?aid=390156&label=duc511jc-1FCAYYiwQosQFCEHNlbHZhLWRlLWlxdWl0b3NIM1gDaMABiAEBmAEJuAEZyAEM2AEB6AEB-AENiAIBqAIDuALJiMuSBsACAdICJDZjMWEyMDgyLTcxZGQtNDY5OC05MWRkLWZkNDU4MDRlMmM2NtgCBuACAQ&sid=2fc548bdc58e09c25e507d7d14833da1&all_sr_blocks=200965602_116124559_0_21_0&checkin=2022-06-03&checkout=2022-06-06&dest_id=13532&dest_type=region&dist=0&group_adults=2&group_children=0&hapos=21&highlighted_blocks=200965602_116124559_0_21_0&hpos=21&matching_block_id=200965602_116124559_0_21_0&no_rooms=1&req_adults=2&req_children=0&room1=A%2CA&sb_price_type=total&sr_order=popularity&sr_pri_blocks=200965602_116124559_0_21_0__90000&srepoch=1649996590&srpvid=dc241ed66b100039&type=total&ucfs=1&activeTab=main
   - walkways, flooded, pool, Ayahuasca
   - offer: $375 / pers
** Ayurasca
  - https://theotorongocenter.com/
** Camping Trips
   - https://www.lupunajungletours.com/
   - https://www.machutravelperu.com/blog/manu-national-park
** Emails
*** Inca trail
Hi,

My name is Julian and I am interested in 5D/4N Inca trail tour after 25th of May

Can you please send us a group offer for 6 people? and if you have availability at end of May?

Thank you!
--
Julian
*** Amazon lodge
Hi,

My name is Julian and we are a group of 6 people (3 couples ~40 years old) from Romania and we are interested in booking a 4D/3N program.

Can you please check if you have availability between 3 and 6 June (4 days / 3 nights) and send us a group offer for 6 people, 3 bungalows or double rooms.

On the last day, is there any way to leave 1h/2h earlier, we want to catch a plane to Lima at 17:40?

Also, I know June is the end of the rainy season, will the lodge be completely flooded or can we also walk on the ground?

Thank you!
--
Julian
** Articles
  - https://www.imperatortravel.ro/2016/09/vacanta-la-peste-3000-metri-ep-8-machu-picchu-orasul-magic.html
  - https://www.imperatortravel.ro/2013/08/trei-saptamani-in-peru-ep-4i-believe-in-miracles-machu-picchu-cusco-si-valea-sacra.html
  - https://www.wildjunket.com/things-to-do-in-cusco-peru/
  - https://www.manitiexpeditions.com/what-to-bring/
** Altitude sickness
   - as reference [[https://en.wikipedia.org/wiki/Aiguille_du_Midi][Aiquille du Midi]] is at 3800m altitude then I climbed Mont Blanc at 4800m altitude, I felt OK, no altitude sickness
